/* Imports */

import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import * as errorHandler from 'errorhandler';
import * as dotenv from 'dotenv';

const nodeEnv = process.env.NODE_ENV || 'dev';

/* internal */
import Controller from './controllers/controller';

/* ENV Config */
dotenv.config({ path: `./config/environments/${nodeEnv}.env` });

/* Express configuration. */
const app = express();
app.set('port', process.env.PORT || 3000);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* Routes */
const controller = new Controller();

// Add routes here, and bind to controller action
app.get('/api/v1/controller/action', controller.action.bind(controller));

if (nodeEnv === 'dev') {
  // only use in development
  app.use(errorHandler());
}
/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('  App is running at http://localhost:%d in %s mode', app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
