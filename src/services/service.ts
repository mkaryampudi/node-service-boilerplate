import { Model } from '../models/model';
import request = require('request-promise');
import * as _ from 'lodash';

export default class Service {

  constructor(
    private var1: string,
    private var2: string
  ) {}

  public getObject() {
    return {
      someField: this.var1,
      anotherField: this.var2
    } as Model;
  }
}
