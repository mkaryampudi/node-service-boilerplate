import { Response, Request } from 'express';
import { Model } from '../models/model';
import Service from '../services/service';
import * as _ from 'lodash';

export default class Controller {
  private service: Service;

  constructor() {
    this.service = new Service('Hello', 'Goodbye');
  }

  public action(req: Request, res: Response): any {
    // Do something as a result of the controller action
    const obj: Model = this.service.getObject();

    // Return JSON response and status
    res.status(200).json({
      status: 'success',
      message: 'Hello world!',
      object: obj
    });
  }
}
