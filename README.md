# Boilerplate Node.js Service

Use this as a boilerplate/starting point for a Node.js service. It comes with a basic structure of a service, as well as essential development tools such as:

- nodemon - automatically restarts the server on code change
- gulp - automatically compiles TypeScript code to JavaScript code on code change
- Istanbul/nyc - generates unit test coverage reports (can be found in `coverage/index.html` after running `npm test`)
- tslint - checks your code for syntax errors. A plugin is required for your editor in order to see the lint errors.
- EditorConfig - enforces code style rules

## Instructions
1. Fork this repo, and clone to local machine.
1. Install dependencies: `npm install`.
1. Run gulp to compile TypeScript to JavaScript: `gulp`.
1. Open a new terminal window and run nodemon to start server and have it automatically reload on code change: `npm run nodemon`
1. The boilerplate has a dummy API endpoint. Make a `GET` request to `http://localhost:3000/api/v1/controller/action`. The expected response should be similar to the following:
```
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 101
Content-Type: application/json; charset=utf-8
Date: Mon, 21 Aug 2017 22:42:00 GMT
ETag: W/"65-WS+pbu4Qtnxl3TterkihoW2xU5M"
X-Powered-By: Express

{
    "message": "Hello world!",
    "object": {
        "anotherField": "Goodbye",
        "someField": "Hello"
    },
    "status": "success"
}
```
PROTIP: [HTTPie](http://httpie.org) is a great tool for testing API endpoints. It uses `curl` under the hood. Another great tool is [Postman](http://www.getpostman.com).

To run tests, run `npm test`. A coverage report should be generated and can be found in `coverage/index.html`.
