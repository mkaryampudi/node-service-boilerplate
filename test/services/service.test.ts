import * as mocha from 'mocha';
import * as chai from 'chai';
import nock = require('nock');

import Service from '../../src/services/service';

import { Model } from '../../src/models/model';

const expect = chai.expect;

describe('#getObject()', () => {

  it('should respond correctly', () => {
    const service = new Service('Foobar1', 'Foobar2');

    const response: Model = {
      anotherField: 'Foobar2',
      someField: 'Foobar1'
    };

    expect(service.getObject()).to.eql(response);
  });

});