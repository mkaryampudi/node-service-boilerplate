import * as mocha from 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');
import nock = require('nock');
import url = require('url');

import server = require('../../src/server');

chai.use(chaiHttp);
const expect = chai.expect;

describe('#action()', () => {

  it('responds successfully', () => {

    return chai.request(server)
      .get('/api/v1/controller/action')
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body.status).to.eql('success');
        expect(res.body.message).to.eql('Hello world!');
        expect(res.body.object).to.eql({
          anotherField: 'Goodbye',
          someField: 'Hello'
        });
      });
  });

});
